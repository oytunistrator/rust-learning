use std::thread::sleep;
use std::time::Duration;

fn main(){
    let tenten = Duration::new(3, 0);
    let mut i = 0;

    loop{
        i = i + 20;
        print!("{}[2J", 27 as char);
        println!("Loading [{}]...", i);
        if i == 100 { 
            println!("DONE!");
            break;
        }
        sleep(tenten);
    }
}