extern crate rustc_serialize;
#[macro_use] extern crate nickel;

use std::io::Write;
use nickel::status::StatusCode::NotFound;
use nickel::{Nickel, NickelError, Action, Continue, Halt, Request, JsonBody};
use std::collections::HashMap;




#[derive(RustcDecodable, RustcEncodable)]
struct Person {
    firstname: String,
    lastname:  String,
}

fn main() {
    let mut server = Nickel::new();    

    //this is how to overwrite the default error handler to handle 404 cases with a custom view
    fn custom_404<'a>(err: &mut NickelError, _req: &mut Request) -> Action {
        if let Some(ref mut res) = err.stream {
            if res.status() == NotFound {
                let _ = res.write_all(b"<h1>404!</h1> <p>Route doesnt exist!</p>");
                return Halt(())
            }
        }

        Continue(())
    }


    // issue #20178
    let custom_handler: fn(&mut NickelError, &mut Request) -> Action = custom_404;

    server.utilize(router! {
        get "/ping" => |_req, _res| {
            "Pong!"
        }

        get "/hello" => |_req, _res| {
            "Hello!"
        }

        post "/person" => |_req, _res| { 
            let person = _req.json_as::<Person>().unwrap();
            format!("Hello {} {}", person.firstname, person.lastname)
        }

        get "/template/:filename" => |_req, _res| {
            let mut data = HashMap::new();
            data.insert("filename", _req.param("filename"));
            return _res.render("template/test.tpl", &data);
        }

        get "/ip" => |_req, _res|{
            format!("{}", _req.origin.remote_addr)
        }
    });

    server.handle_error(custom_handler);

    server.listen("127.0.0.1:6767");
}