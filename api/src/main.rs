extern crate iron;
extern crate rustc_serialize;

use std::collections::HashMap;
use iron::prelude::*;
use iron::{Handler};
use iron::status;
use iron::mime::Mime;
use rustc_serialize::json;

struct Router {
    // Routes here are simply matched with the url path.
    routes: HashMap<String, Box<Handler>>
}

impl Router {
    fn new() -> Self {
        Router { routes: HashMap::new() }
    }

    fn add_route<H>(&mut self, path: String, handler: H) where H: Handler {
        self.routes.insert(path, Box::new(handler));
    }
}

impl Handler for Router {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        match self.routes.get(&req.url.path().join("/")) {
            Some(handler) => handler.handle(req),
            None => Ok(Response::with(status::NotFound))
        }
    }
}

#[derive(RustcDecodable, RustcEncodable)]
struct Person {
    id: u8,
    name: String,
    surname: String
}

fn main() {
    let mut router = Router::new();

    router.add_route("api".to_string(), |_: &mut Request| {
        let person = Person {
            id: 1,
            name: "John".to_string(),
            surname: "Condor".to_string()
        };
        let encoded = json::encode(&person).unwrap();     
        let content_type = "application/json".parse::<Mime>().unwrap();
        Ok(Response::with((content_type, status::Ok, encoded)))
    });

    router.add_route("error".to_string(), |_: &mut Request| {
       Ok(Response::with(status::BadRequest))
    });

    Iron::new(router).http("localhost:3000").unwrap();
}