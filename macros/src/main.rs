macro_rules! log {
    ($msg:expr) => {{
        println!("(L): {}", $msg);
    }};
}

macro_rules! msg {
    ($msg:expr) => {{
        println!("(M): {}", $msg);
    }};
}

macro_rules! info {
    ($msg:expr) => {{
        println!("(I): {}", $msg);
    }};
}

macro_rules! negative_msg {
    ($msg:expr) => {{
        println!("(-): {}", $msg);
    }};
}

macro_rules! positive_msg {
    ($msg:expr) => {{
        println!("(+): {}", $msg);
    }};
}

macro_rules! write_html {
    ($w:expr, ) => (());

    ($w:expr, $e:tt) => (write!($w, "{}", $e));

    ($w:expr, $tag:ident [ $($inner:tt)* ] $($rest:tt)*) => {{
        write!($w, "<{}>", stringify!($tag));
        write_html!($w, $($inner)*);
        write!($w, "</{}>", stringify!($tag));
        write_html!($w, $($rest)*);
    }};
}

fn main(){
    use std::fmt::Write;
    let mut out = String::new();

    //dotto
    if cfg!(target_os = "macos") || cfg!(target_os = "ios") {
        println!("Think Different!");
    }

    log!("LOG TEST!");
    msg!("I AM SO LAME!");
    info!("I AM INFO MESSAGE...");
    negative_msg!("NEGATIVE");
    positive_msg!("POSITIVE");

    write_html!(&mut out,
        html[
            head[title["Macros guide"]]
            body[h1["Macros are the best!"]]
        ]);

    positive_msg!(out);
}